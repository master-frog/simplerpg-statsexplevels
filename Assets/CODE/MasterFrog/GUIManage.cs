﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


namespace UnityCommunityProject
{
    public class GUIManage : MonoBehaviour
    {

        [Header("REFERENCES")]
        public Text HP_Text;
        public Text HP_Shadow_Text;
        public Text Exp_Text;
        public Text Exp_Shadow_Text;
        public Text Level_Text;
        public Text Level_Shadow_Text;

        public PlayerStats PlayerStats;

        [Header("VARIABLES")]
        public int n;

        [Header("FLAGS")]
        public bool tf;

        void Start()
        {

        }

        void Update()
        {
            HP_Shadow_Text.text = HP_Text.text = "HP " + PlayerStats.GetCurrentHP().ToString();
            Exp_Shadow_Text.text = Exp_Text.text = "Exp " + PlayerStats.GetCurrentLevelExp().ToString() + "/" + PlayerStats.GetNextLevelExp().ToString();
            Level_Shadow_Text.text = Level_Text.text = "Level " + PlayerStats.GetCurrentLevel().ToString();
        }
    }
}