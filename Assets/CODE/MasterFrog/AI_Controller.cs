﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


namespace UnityCommunityProject
{
    public class AI_Controller : MonoBehaviour
    {

        [Header("REFERENCES")]
        public Transform Player;

        [Header("VARIABLES")]
        public float Player_Follow_Distance; // How far enemy will follow player from
        public float Leash_Distance; // How far this enemy will travel from its starting point before abandoning the hunt
        public float MaxSpeed;
        public float MovementAcceleration;
        Vector2 MovementAxis;
        Vector3 StartPosition;

        [Header("FLAGS")]
        public bool ReturningToStartPosition;

        void Start()
        {
            Player = GameObject.Find("Player").transform;
            StartPosition = transform.position;
        }

        void Update()
        {
            if (Vector3.Distance(transform.position, StartPosition) < Leash_Distance)
            {
                if (ReturningToStartPosition)
                {
                    TrackTowardPosition(StartPosition);
                    if (Vector3.Distance(transform.position, StartPosition) < 3)
                    {
                        ReturningToStartPosition = false;
                    }
                }
                else {
                    if (Vector3.Distance(transform.position, Player.position) < Player_Follow_Distance)
                    {
                        TrackTowardPosition(Player.position);
                    }
                    else {
                        ReturningToStartPosition = true;
                    }
                }
            }
            else {
                ReturningToStartPosition = true;
                TrackTowardPosition(StartPosition);
            }
        }

        public void TrackTowardPosition(Vector3 Destination)
        {
            if (Destination.x < transform.position.x)
            {
                MovementAxis.x = -MaxSpeed;
            }
            else if (Destination.x > transform.position.x)
            {
                MovementAxis.x = MaxSpeed;
            }
            else {
                MovementAxis.x = 0;
            }

            if (Destination.z < transform.position.z)
            {
                MovementAxis.y = -MaxSpeed;
            }
            else if (Destination.z > transform.position.z)
            {
                MovementAxis.y = MaxSpeed;
            }
            else {
                MovementAxis.y = 0;
            }

            if (Vector3.Distance(transform.position, Destination) < 1)
            {
                MovementAxis.x = 0;
                MovementAxis.y = 0;
            }
        }

        public float GetHorizontalAxis()
        {
            return MovementAxis.x;
        }

        public float GetVerticalAxis()
        {
            return MovementAxis.y;
        }
    }
}